#!/bin/sh
cd "$(dirname "$0")" || exit 1

. /etc/os-release || exit 1
if [ "$ID" = debian ]; then
	xargs sudo apt install < ./necessary-packages-debian.txt
elif [ "$ID" = arch ]; then
	sudo pacman -S --needed $(cat ./necessary-packages-arch.txt)
fi
