#!/bin/bash
config_home="${XDG_CONFIG_HOME:-$HOME/.config}"
cache_home="${XDG_CACHE_HOME:-$HOME/.cache}"
data_home="${XDG_DATA_HOME:-$HOME/.local/share}"
state_home="${XDG_STATE_HOME:-$HOME/.local/state}"

cd "$(dirname "$0")" || exit 1

install_file() {
	echo "Copying $2..."
	mkdir -p "$(dirname "$2")"
	[ -f "$2" ] && diff --color=auto "$2" "$1"
	cp "$1" "$2"
}

echo "Installing configs..."
find ./config/ -type f -print0 |
	while IFS= read -r -d '' line; do
		install_file "$line" "${line/.\/config/$config_home}"
	done

sed -i "s/USERNAME/${USER}/"		"${config_home}/deemix/config.json"
sed -i "s/USERNAME/${USER}/"		"${config_home}/mpd/mpd.conf"
sed -i "s|CONFIG_HOME|${config_home}|"	"${config_home}"/easyeffects/output/*.json
sed -i "s|STATE_HOME|${state_home}|"	"${config_home}/wgetrc"
sed -i "s|CACHE_HOME|${cache_home}|"	"${config_home}/ncmpcpp/config"

echo "Installing dotfiles..."
for f in ./dotfiles/*; do
	install_file "$f" ~/."${f/.\/dotfiles\//}"
done

if command -v flavours >/dev/null; then
	ln -sf "${config_home}/flavours/templates/sway/templates/sandelinos.mustache" "${data_home}/flavours/base16/templates/sway/templates/sandelinos.mustache"
	ln -sf "${config_home}/flavours/templates/alacritty/templates/toml-256.mustache" \
		"${data_home}/flavours/base16/templates/alacritty/templates/toml-256.mustache"
	ln -sf "${config_home}/flavours/templates/fuzzel" "${data_home}/flavours/base16/templates/fuzzel"
	echo "Applying Flavours..."
	flavours apply "$(flavours current)"
fi

if [ -f "$config_home/MusicBrainz/Picard.ini" ]; then
	echo "Configuring Picard..."
	sed -i 's/caa_image_size.*/caa_image_size=-1/g' ~/.config/MusicBrainz/Picard.ini
fi

[ -f "${HOME}/.wget-hsts" ] && mv "${HOME}/.wget-hsts" "${state_home}/wget-hsts"
[ -f "${HOME}/.mysql_history" ] && mv "${HOME}/.mysql_history" "${state_home}/mysql_history"

if command -v alacritty > /dev/null; then
	sed -i 's/TERMINAL/alacritty/' "${config_home}/sway/config"
elif command -v foot > /dev/null; then
	sed -i 's/TERMINAL/foot/' "${config_home}/sway/config"
else
	echo "no valid terminal found"
fi

# Add HEVC to MPV ytdl-format when hardware decoding support is present
if vainfo | grep 'VAProfileHEVCMain.*VAEntrypointVLD' >/dev/null ; then
	sed -i 's/avc|h264/hevc|h265|avc|h264/' "${config_home}/mpv/mpv.conf"
fi

# Make celluloid use MPV's config file
(command -v celluloid && command -v gsettings) >/dev/null && {
	gsettings set io.github.celluloid-player.Celluloid mpv-config-enable true
	gsettings set io.github.celluloid-player.Celluloid mpv-config-file "file://${config_home}/mpv/mpv.conf"
}

echo "Installing scripts..."
for f in ./scripts/*; do
	dest="$HOME/.local/bin/${f/.\/scripts\//}"
	install_file "$f" "$dest"
	chmod +x "$dest"
done

echo "Installing .desktop entries..."
for f in ./desktop/*.desktop; do
	desktop-file-install --dir="$HOME/.local/share/applications" "$f"
	sed -i "s|HOME|$HOME|" "$HOME/.local/share/applications/${f/*\//}"
done
echo "Updating desktop database..."
update-desktop-database
echo "Setting up sandelinos-default-browser..."
xdg-mime default sandelinos-default-browser.desktop x-scheme-handler/http
xdg-mime default sandelinos-default-browser.desktop x-scheme-handler/https

command -v gsettings > /dev/null && gsettings set org.gnome.desktop.interface color-scheme prefer-dark
