## Install necessary packages

    ./install-packages.sh

## Install configs

    ./install.sh

## extra packages (not necessary)
* light
* mpd
* ncmpcpp
* mpc
* pactl
    * Debian: pulseaudio-utils
    * Arch: libpulse
* flavours
    * Arch: AUR
    * Debian: not packaged

### nextcloud
* nextcloud-client
* python-nautilus
* nextcloud-client-cloudproviders
